# toshiba-220CS
 

## Quick Info

msdos from IDE 

grub 2.04 


netbsd 7 working with  "netbsd 7   i386, 7.1.1"


antic hda2  slackware 10 (usb, sdb working) !

and also plpbt from hd0,msdos1.

1.4GB IDE (into the main unit.)

usb 1.x. plpbt is the plop boot manager, which requires usb 1.1.
usb 1.1 is ohci, usb 1.0 is uhci.





## Toshiba 220CS Satellite, 1.4GB IDE hda disk. 

![](media/Toshiba_Satellite_220CS-9075.jpg)



## ANTIC PCKIT 

https://gitlab.com/openbsd98324/antic-pckit/

![](https://gitlab.com/openbsd98324/antic-pckit/-/raw/main/media/antic-pckit-logo.png)


## Content Toshiba 

1.) Grub 2.04

2.) partition 1, with Netbsd 7

3.) partition 2, with slackware 10 !

## How to use Qemu to install netbsd and more 

This live image is a NetBSD (amd64), which allows to install GRUB 2.

wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/netbsd-live/-/raw/master/live/9.1/amd64/v5/netbsd-live-image-2000mb-v5.img.gz"


qemu-system-x86_64    -machine type=pc     -drive file=netbsd-live-image-2000mb-v5.img,index=0,media=disk,format=raw  -drive file=image-sdb-toshiba-220cs-1.4G-disk-1.img,index=1,media=disk,format=raw  -boot c



## How to install it.

1.) manage to copy the netbsd 7 - pc medkit for old PC (version 2) onto the /dev/hda or unscrew and use dd to get a first NetBSD 7 system. 

https://gitlab.com/openbsd98324/pcmedkit

2.) have /netbsd-install.gz (netbsd7), install with netbsd-install.gz onto the IDE disk, netbsd 7

https://gitlab.com/openbsd98324/netbsd-7

Put with netbsd 9.1, the grub using i386, nota. it is already avaialble in pcmedkit version 2.

https://gitlab.com/openbsd98324/pcmedkit

3.) Once installed sd0 (usb) will work only with the /netbsd-install.gz 

4.) Add the second partition using netbsd 7

The MBR with table is located here: https://gitlab.com/openbsd98324/toshiba-220cs/-/raw/main/mbr-disk/mbr.mbr
partition 1, netbsd
and partition 2, linux ext2fs (ext2)

fdisk wd0

disklabel -i wd0

add e.) to Linux Ext2fs 

5.) newfs_ext2fs /dev/rwd0e 

and copy from the sd0a (on the running netbsd-install.gz) the rootfs to the partition #2 (aka wd0e):
https://gitlab.com/openbsd98324/slackware-10.0/-/raw/main/slackware-10.0-base-i386-base-rootfs.tar.gz


6.) Boot 

set root=(hd0,msdos1)

linux16 /boot/vmlinuz  root=/dev/hda2  ro

boot


make sure that the /etc/fstab   gives /dev/hda2 .


7.) Have Fun, MultiBoot NetBSD and Slackware Linux !

## Boot FreeDOS

Use the GRUB of NetBSD 7.1.1., installed 2.04, on it msdos1, by using memdisk:


*Image Floppy:*
 
set root=(hd0,msdos1)

linux16 /root/freedos/memdisk/memdisk raw 

initrd16 /root/freedos/fdos1440.img



*Image CDROM:*
 
set root=(hd0,msdos1)

linux16 /root/freedos/memdisk/memdisk iso 

initrd16 /root/freedos/fdbasecd.iso

Source code: https://gitlab.com/openbsd98324/memdisk


## Manual (PDF)

https://gitlab.com/openbsd98324/toshiba-220cs/-/raw/main/satellite_220_e.pdf


## Note  

1.) debian etch 4. does not boot but crashes with grub 0.97 and grub 2.04  on the 220CS.

2.) Netbsd 7.1.1. works fine, usb works, and it is fast enough.

3.) Netbsd 8.0 i386 boots, but it is quite slow.

4.) By default it is recommended windows 95 by manufacturer. It can also run Windows 98. Tested with win98 as well. 




